void permutation(int a, int b){
    int tmp = a;
    a = b;
    b = tmp;
}

void permutation(int *a, int *b){
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void permutation2(int &a, int &b){
    int tmp = a;
    a = b;
    b = tmp;
}


int maxivect(int tab[], int t) {
    int maximum = tab[0];
    for(int i=1; i < t; i++){
        if(tab[i] > maximum){
            maximum = tab[i];
        }
    }
    return maximum;
}
