## COURS C++ AVANCE IFRI M1

## RAPPELS

---

## TP1 :

Soit la classe :

```
    +----------------------------+
    |          AdresseIP         |
    |----------------------------|
    |           octet1           |
    |           octet2           |
    |           octet3           |
    |           octet4           |
    |----------------------------|
    |                            |
    +----------------------------+

```

## TAF :

1. Créer un projet C++;
2. Créer un fichier .h ou .hpp dans lequel vous déclarerez la classe AdresseIP
3. Creer un fichier .cpp dans lequel vous definirez les methodes de la classe
4. Creer un fichier main.cpp dans lequel vous allez creer un objet de cette classe.

---

## TP2

Soit le menu

Menu

1. Somme
2. Produit
3. Division
4. Quitter

## TAF:

1. Dans le fichier main, creer le menu
2. Dans le fichier fonction.cpp, creer les fonctions somme, produit et Division
3. Appeler ces fonctions dans la fonction main pour rendre opérationnel le menu

---

## TP3 :

Ecrire une fonction qui determine la valeur maximale d'un vecteur d'entiers.
Dans la fonction main, déclarez un vecteur, chargez le vecteur puis utilisez la fonction ecrite pour determiner la valeur maximale du vecteur.
